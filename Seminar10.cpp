﻿#include <iostream>

class Vector
{
public:
	Vector() : x(2), y (3), z(5)
	{};
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	double GetVector() 
	{
		return(x, y, z);
	}

	void SetVector(double newX, double newY, double newZ) 
	{
		x = newX;
		y = newY;
		z = newZ;
	}

	double VectorLength()
	{
		return std::sqrt(x * x + y * y + z * z);
	}

private:
	double x;
	double y;
	double z;
};


int main()
{
	setlocale(LC_ALL, "Rus");
	Vector v;
	std::cout << "Длина вектора: " << v.VectorLength() << std::endl;
	return 0;
}

